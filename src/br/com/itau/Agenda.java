package br.com.itau;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class Agenda {

    List<Contato> contatos;

    public Agenda() {
        contatos = new ArrayList<>();
    }

    public void adicionarContato() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Quantas pessoas deseja adicionar ao iniciar o sistema? ");
        int numeroPessoas = scanner.nextInt();


        for (int i = 1; i < numeroPessoas + 1; i++) {

            System.out.print("Nome do " + i + "° Contato: ");
            String nome = scanner.next();

            System.out.print("Email do " + i + "° Contato: ");
            String email = scanner.next();

            System.out.print("Telefone do " + i + "° Contato: ");
            String telefone = scanner.next();

            Contato contato = new Contato(nome, email, telefone );

            contatos.add(contato);
        }

        System.out.println("Inclusão com Sucesso!");

    }

    public void removerContato() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite 1 para remover o contato por email ou 2 para remover pelo numero");
        int opcao = scanner.nextInt();

        if (opcao == 1) {
            System.out.println("Remoção do contato por email. ");
        } else {
            System.out.println("Remoção do contato por telefone. ");
        }
//        contatos.remove(contato);


    }

    public void mostrarAgenda() {
        for (Contato contato : contatos) {
            System.out.println(contato.getNome() + " " + contato.getEmail() + " " + contato.getTelefone());
        }
    }


}
